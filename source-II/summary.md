---
header-includes: |
  \makeatletter
  \def\input@path{{source/}}
  \input{latex-headers.tex}
---

\newpage

# Summary

## Solvability

A finite group $G$ is said to be **solvable** if one of the following equivalent
conditions hold

* There exists a subnormal series with *abelian* subquotients.
* There exists a subnormal series with *cyclic* subquotients.
* The derived series of $G$ terminates at the trivial group.

**Theorem.** *The general polynomial of degree $d$ is solvable if and only if
the symmetric group on $d$ elements $S_d$ is a solvable group.*
