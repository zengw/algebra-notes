\setcounter{chapter}{6}
\chapter{Constructions with a Straightedge and
Compass}\label{lecture:constructions}

The goal of this lecture will be to describe the set of complex numbers that are
constructible using a straightedge and compass.

\begin{remark}
  A \textbf{straightedge} refers to a ruler without markings.
\end{remark}

\section{Basic Operations and Constructions}

Let $\E$ denote the Euclidean Plane. Beginning with as set of points $S
\subseteq \E$, it is of interest what further points can be constructed with
these points. We will call the initial set of points along with any points which
have already been constructed \textit{known} points, whereas \textit{arbitrary}
points are, in general, non-constructible points.

\subsection{Operations}

The set of legal \textit{operations} defined on known points are consists of

\begin{enumerate}[(1)]
  \item Creating a straight line between two known points;
  \item Taking the distance between two known points, and forming a circle of
    that distance around a third known point;
  \item Creating new points at intersections of previously drawn lines or
    circles.
\end{enumerate}


\subsection{Constructions}

From these operations we immediately get a set of legal \textit{constructions}.
These are

\begin{enumerate}[(a)]
  \item Transfer a length onto a line segment;
  \item Transfer an angle onto a line segment;
  \item Construct arbitrarily large lengths by repeating (a);
  \item Find the midpoint of two points;
  \item Construct the perpendicular bisector of a line segment;
  \item Construct a perpendicular line to a given line through a given point;
  \item Construct a parallel line.
\end{enumerate}

\section{Constructible Numbers}

We will from now on denote by $K_S$ the set of points which can be constructed
by the basic operations (1)-(3). From now on, we will assume $|S| > 1$, as
otherwise $K_S = S$.

\begin{definition}[Constructible length, Constructible angle]
  For $A, B, C \in K_S$, the Euclidean distance $\metric (A, B) \in R_{\geq
  0}$ is called an \textbf{S-Constructible length}; the angle $\measuredangle
  ABC$ is called an \textbf{S-Constructible angle}.
\end{definition}

\begin{remark}
  When $S=\set{0,1}$, we will only refer to ``Constructible lengths'' and
  ``Constructible angles'', omitting the ``S-''.
\end{remark}

This is all still a very geometric way of thinking about these problems, we will
now translate them into algebraic terms. To do this, we will identify $\E \cong
\R^2$ equipped with the Euclidean norm, such that $(0, 0)^{\mathrm{T}}, {(1,
0)}^{\mathrm{T}} \in S$. We can do this, as we have assumed $|S| > 1$ and we can
choose the coordinates arbitrarily. We are now ready to make our first
observation.

\begin{remark}
  For ease of notation we will henceforth drop the transpose on coordinates.
\end{remark}

\begin{fact}\label{fact:thefirst}
  $\forall n \in \Z : (0, n), (n, 0) \in K_S$
\end{fact}

\begin{proof}~
  \begin{itemize}
    \item To construct $(n, 0)$, transfer the length $\metric ((0,0), (0,1)) =
      1$ n times by construction (a) $n$ times.
    \item To construct $(0, n)$ create a perpendicular bisector to the line
      segment $(0, 0), (0,1)$ going through $(0, 0)$. Then transfer the length
      $1$ onto this line $n$ times.
  \end{itemize}
\end{proof}

However, as we are seeking an algebraic description of these problems, we still
don't have enough structure on $\R^2$. So, instead, let's identify $\E \cong
\R^2 \cong \C$ with the usual mapping $(x,y) \in \R^2 \mapsto x+\ii y \in \C$.
We hope that we can then use the field properties of $\C$ to better describe
these constructions. Now without loss of generality we have $0, 1 \in S
\subseteq \C$, which by fact~\ref{fact:thefirst} implies $\Z, i\Z \in K_S$.

\begin{theorem}[$K_S$ is a field]
  We claim that $K_S$ is the smallest subfield of $\C$ with the
  properties
  \begin{enumerate}[(i)]
    \item Contains $S$.
    \item Closed under complex conjugation.
    \item Closed under square root extraction.
  \end{enumerate}
\end{theorem}

Before we prove this theorem, we will introduce three lemmas

\begin{lemma}[Constructible Lengths form a multiplicative group]
  As $(0,1) \in K_S$ by assumption, 1 is a constructible length. Furthermore,
  let $a, b, c$ be constructible lengths. Then $\frac{bc}{a}$ is also a
  constructible length.  In particular for any $a, b$ constructible lengths,
  $ab$ is also constructible, and $\frac{a}{b}$ is also constructible.
\end{lemma}

\begin{proof}
  We can exploit similar triangles to prove this. The points $(a, 0), (b, 0),
  (a,c) $ are all constructible,
    \begin{center}
      \begin{tikzpicture}
        [
          scale=1,
          >=stealth,
          point/.style = {draw, circle,  fill = black, inner sep = 1pt},
          dot/.style   = {draw, circle,  fill = black, inner sep = 0.2pt},
        ]

        \def\a{3}
        \def\b{4.5}
        \def\c{1.5}

        % The Axes
        \node (origin) at (0,0)
          [point, label = {below right:$\mathcal{O}$}]{};

        \draw[->] (-0.5,0) -- (\b + 1, 0);
        \draw[->] (0,-0.5) -- (0,\b * \c / \a +1);

        % Points
        \node (n1) at (\a, 0)           [point, label = {below:$a$}]{};
        \node (n2) at (\b, 0)           [point, label = {below:$b$}]{};
        \node (n3) at (\a, \c)
              [point, label = {below right:$(a, c)$}]{};
        \node (n4) at (\b, \b*\c / \a)  [point] {};
        \node (n5) at (0, \c)           [label = {left:$c$}] {};
        \node (n6) at (0, \b * \c / \a) [label = {left:$\frac{bc}{a}$}] {};

        % Line connecting
        \draw[-] (0,0)     -- (n4);
        \draw[-] (n1)      -- (n3);
        \draw[-] (n2)      -- (n4);
        \draw[dashed] (n5) -- (n3);
        \draw[dashed] (n6) -- (n4);
      \end{tikzpicture}
    \end{center}

  Setting either $a=1$ or $c=1$ we obtain the desired result. Also setting $b =
  c = 1$ shows that $\frac{1}{a}$ is constructible.
\end{proof}

\begin{lemma}[Constructible Angles are closed under addition]
  Moreover, let $\alpha, \beta$ be constructible angles. Then $|\alpha \pm
  \beta|$ is also a constructible angle.
\end{lemma}

% Need NBS after proof call to correct for text appearing below figure.
\begin{proof}[Proof by picture] ~
  \begin{center}
    \begin{tikzpicture}
        [%
          scale=1,
          >=stealth,
          point/.style = {draw, circle,  fill = black, inner sep = 1pt},
          dot/.style   = {draw, circle,  fill = black, inner sep = 0.2pt},
        ]

      \def\rad{4}

      % Note \a must be greater than \b
      \def\a{63}
      \def\b{23}
      \def\offset{20}

      \node (n0) at (0,0) [point, label = {below right:$\mathcal{O}$}] {};
      \node (n1) at +(\offset:\rad) {};
      \node (n2) at +(\offset+\a:\rad) {};
      \node (n3) at +(\offset+\a+\b:\rad) {};
      \node (n4) at +(\offset+\a-\b:\rad) {};

      \draw
        (n1) -- (n0) -- (n2)
        pic[%
          "$\alpha$",
          draw=orange,
          <->,
          angle eccentricity=1.2,
          angle radius=1cm,
        ]{angle=n1--n0--n2};

      \draw
        (n2) -- (n0) -- (n3)
        pic[%
          "$\beta$",
          draw=orange,
          <->,
          angle eccentricity=1.2,
          angle radius=1cm
        ]{angle=n2--n0--n3};

      \draw
        (n1) -- (n0) -- (n3)
        pic[%
          "$\beta+\alpha$",
          draw=orange,
          <->,
          angle eccentricity=1.2,
          angle radius=2cm
        ]{angle=n1--n0--n3};

      \draw
        (n1) -- (n0) -- (n4)
        pic[%
          "$\beta-\alpha$",
          draw=orange,
          <->,
          angle eccentricity=1.2,
          angle radius=3cm
        ]{angle=n1--n0--n4};

    \end{tikzpicture}
  \end{center}
\end{proof}

\begin{lemma}
  A number $z = re^{\ii \alpha} \in \E$ is constructible if and only if $r$ and
  $\alpha$ are respectively a constructible length and angle.
\end{lemma}

\begin{proof}[Proof of theorem]
  First we will prove that $K_S$ is a field. For this, we will prove that $(K_S,
  +, 0)$, and $(K_S, \cdot, 1)$ are groups.

  \begin{siderules}
    \begin{theorem}[$(K_S, +, 0)$ is a group]
    \end{theorem}

    \begin{proof}
      By assumption $0 \in S$. To verify closure under addition, we can do a
      short case evaluation:
      \begin{enumerate}
        \item  \textit{If} $zw = 0$, then without loss of generality $w = 0$ and
          $w + z = z \in K_S$

        \item \textit{if} $z, w$ are collinear, then we can draw a circle around
          the point with a greater norm, with radius of the smaller norm.  Then
          the intersection of the line going through the origin and $w$ with the
          greatest norm is the sum of $w$ and $z$;

          \begin{center}
            \begin{tikzpicture}
              [
                scale=1,
                >=stealth,
                point/.style = {draw, circle,  fill = black, inner sep = 1pt},
                dot/.style   = {draw, circle,  fill = black, inner sep = 0.2pt},
              ]

              % The circle
              \def\a{1.5}
              \def\b{2.5}
              \def\g{30}

              % The Axes
              \node (origin) at (0,0)
              [ point, label = {below right:$\mathcal{O}$}]{};

              % Points
              \node (n0) at (0,0)
                [point, label = {below right:$\mathcal{O}$}]{};
              \node (n1) at +(\g:\a)
                [point, label = {below:$z$}]{};
              \node (n2) at +(\g:\b)
                [point, label = {below:$w$}]{};
              \node (n3) at +(\g:\a+\b)
                [point, label = {above right:$z+w$}]{};
              \node (n4) at +(\g:\b-\a)
                [point, label = {left:$z-w$}]{};

              % Circle
              \draw (n2) circle (\a);

              % Line connecting
              \draw[-] (n0) -- (n3);
            \end{tikzpicture}
          \end{center}

        \item \textit{else} we can draw a parallelogram with one corner the
          origin, another $w$ and the opposite corner $z$. The last corner will
          be the sum of $z$ and $w$.

          \begin{center}
            \begin{tikzpicture}
              [
                scale=1,
                >=stealth,
                point/.style = {draw, circle,  fill = black, inner sep = 1pt},
                dot/.style   = {draw, circle,  fill = black, inner sep = 0.2pt},
              ]

              \def\rad{10}
              \def\a{1}
              \def\b{2}
              \def\c{2}
              \def\d{0.50}

              % The Axes
              \draw[->] (-0.5,0) -- (3,0);
              \draw[->] (0,-0.5) -- (0,3);

              % Points
              \node (n0) at (0,0)
                    [point, label = {below right:$\mathcal{O}$}] {};
              \node (n1) at (\a, \b) [point, label = {above:$z$}] {};
              \node (n2) at (\c, \d) [point, label = {below:$w$}] {};
              \node (n3) at (\a+\c, \b+\d) [point, label = {above:$z+w$}] {};

              % Line connecting
              \draw[dashed] (n0) -- (n1);
              \draw[dashed] (n0) -- (n2);
              \draw[dashed] (n1) -- (n3);
              \draw[dashed] (n2) -- (n3);
              \draw[-] (n0) -- (n3);
            \end{tikzpicture}
          \end{center}
      \end{enumerate}
      %
      It remains to be shown that we have closure under additive inverses. Let
      $z \in K_S$. Then $-z \in K_S$ by following construction: We know that we
      can draw a circle of radius $|z|$, that has it's centre at the origin.
      Then we can construct the line from $z$ though the origin, and define a
      new point at the intersection of that line and the circle. This will be
      $-z$.
      %
      \begin{center}
        \begin{tikzpicture}
          [
            scale=1,
            >=stealth,
            point/.style = {draw, circle,  fill = black, inner sep = 1pt},
            dot/.style   = {draw, circle,  fill = black, inner sep = 0.2pt},
          ]

          % The circle
          \def\rad{1.5}
          \node (origin) at (0,0) [point, label = {below right:$\mathcal{O}$}]{};
          \draw (origin) circle (\rad);

          % The Axes
          \draw[->] (-1.25*\rad,0) -- (1.25*\rad,0);
          \draw[->] (0,-1.25*\rad) -- (0, 1.25*\rad);

          % Points
          \node (n1) at +(60:\rad) [point, label = above:$z$] {};
          \node (n2) at +(-120:\rad) [point, label = below:$-z$] {};

          % Line connecting
          \draw[dashed] (n2) -- (n1);
        \end{tikzpicture}
      \end{center}
    \end{proof}
  \end{siderules}

  \begin{siderules}
    \begin{theorem}[$(K_S, \cdot, 1)$ is a group]
    \end{theorem}

    \begin{proof}
      By assumption $1 \in S$. Let $z = r e^ {\ii\alpha} \in \C$ and $w = s e^
      {\ii \beta} \in \C$ with $r, s, \alpha, \beta \in \R$, then $wz = rs
      e^{\ii (\alpha + \beta)}$. We have seen that constructible lengths are
      closed under multiplication, so $rs$ is also constructible and that
      constructible angles are closed under addition, so $\alpha + \beta \in S$.
      We may conclude that $wz \in S$ and $(K_S, \cdot, 1)$ is closed under
      multiplication. To verify that every element has an inverse we can again
      exploit the polar coordinate representation of complex numbers: by writing
      $z = r e ^{\ii\alpha} \neq 0$, we have with $r^{-1}$ a constructible
      length and $-\alpha$ a constructible angle, that $z^{-1} = r^{-1} e
      ^{-\ii\alpha} \in K_S$.
    \end{proof}
  \end{siderules}

  We have now shown that $(K_S, +, 0)$ is a group, and that $(K_S, \cdot, 1)$ is
  a group. The compatibility between them is given as in $\C$, and we know that
  these elements can be constructed. We may conlcude that $K_S$ is a field.
  Surely $(K_S)$ contains $S$, we closure under conjugation follows comfortably
  as with $z = re^{\ii \alpha}$ we have $\overline{z} = re^{-\ii \alpha}$ which
  is surely constructible. Moving on to showing that $K_S$ is closed under
  extracting the square root requires one more lemma

  \begin{siderules}
    \begin{lemma}[Thales]
      For $d$ a constructible lenth, $\sqrt{d}$ is also constructible.
    \end{lemma}
  \end{siderules}

  \noindent
  It follows then directly that $\sqrt{z} = \sqrt{r}e^{\ii \frac{1}{2}} \alpha$
  is constructible. It remains to be shown, that $K_S$ is the \textit{smallest}
  such field.

  \begin{siderules}
    \begin{fact}
      We can now state a series of facts.
      \begin{itemize}
        \item The S-Constructible lengths are precisely those in $K_S \cap \R
          _{\scriptstyle \geq 0}$.

        \item For all $x, y \in \R: z = x + \ii y \in K_S \iff x, y \in K_S \cap
          \R$.

        \item The line passing through two points $a + \ii b \neq c + \ii d$ is
          given by
          \begin{align*}
            \set{x + \ii y \in \C : x,y \in \R \text{~and~} \det \left (
              \begin{matrix}
                x & a & c \\
                y & b & d \\
                1 & 1 & 1
              \end{matrix}
              \right ) = 0
            }
          \end{align*}

        \item The intersection of two (straight) lines is given by the solution
          to two linear simultaneous equations. (n.b.~straight line as opposed
          to arc from circle)

        \item The intersections of a line and a circle is obtained by elementary
          arithmetic and extraction of square roots. When considering the
          intersection of two circles, we see that the difference of the two
          equations describing the circles is an affine equation; whereas when
          considering the intersection of a line and a circle we are left with a
          quadratic.
      \end{itemize}
    \end{fact}
  \end{siderules}

  \begin{siderules}
    \begin{corollary}\label{cor:constructibility}
      An immediate consequence of the final fact, is that all $z \in K_S$ are
      attained through elementary arithmetic operations, extraction of square
      roots and complex conjugation. We may draw that $K_S$ is the smallest
      field containing all constructible numbers from the set $S$.
    \end{corollary}
  \end{siderules}
\end{proof}

\begin{corollary}
  A number $z \in \C$ is constructible if and only if there exists a tower of finitely
  many field extensions with subextensions of degree two beginning at $\Q$. In
  symbols: there exist $\Q \subseteq K_1 \subseteq \ldots \subseteq K_n$, with
  $z \in K_n$ such that $\dim (K_{i+1}/K_i) = 2$.
\end{corollary}

\begin{proof}
  This is mostly a consequence of corollary~\ref{cor:constructibility}. If all
  $z \in K_S$ are obtained through elementary arithmetic operations, extraction
  of square roots and complex conjugation, they are solutions to quadratic
  equations.

  Suppose we were to go about constructing $z$, we may consider a sequence of
  constructions that culminate in $z$ being in the intersection of any two lines
  or circles, and suppose we index the numbers that are needed for this
  construction in order with a set $I$. Constructing the first number in this
  sequence $z_1$ is equivalent to solving some quadratic equation $p \in \Q[X]:
  p(X) = 0$. Either this polynomial is reducible, then $z_1$ lies in $\Q$ or it
  is irreducible, meaning that $\dim (\Q(z_1) / \Q) = 2$. From here we can
  proceed iteratively and keep adding extensions of degree two until we arrive
  at some field that contains $z$.
\end{proof}

\begin{corollary}
  If $z$ is constructible, then $\dim (K(z) / \Q)$ is a power of two.
\end{corollary}

\section{Applications to Geometric Constructions}

\begin{proposition}
  For $\alpha$ a constructible angle, the following are equivalent
  \begin{itemize}
    \item $\alpha$ is a constructible angle.
    \item $\sin(\alpha)$ is a constructible length.
    \item $\cos(\alpha)$ is a constructible length.
  \end{itemize}
\end{proposition}

\begin{proof}[Proof by picture]~
  \begin{center}
      \begin{tikzpicture}
        [
          scale=1,
          >=stealth,
          point/.style = {draw, circle,  fill = black, inner sep = 1pt},
          dot/.style   = {draw, circle,  fill = black, inner sep = 0.2pt},
        ]

        \def\a{3}
        \def\b{4.5}
        \def\c{1.5}

        % The Axes
        \node (origin) at (0,0)
          [point, label = {below right:$\mathcal{O}$}]{};


        \draw[->] (-0.5,0) -- (\b + 1, 0);
        \draw[->] (0,-0.5) -- (0,\b * \c / \a +1);

        % Points
        \node (n1) at (\a, \c) [point, label = {right:$z$}]{};
        \node (n2) at (\a, 0)  [dot, label = {below:$\cos (\alpha)$}]{};
        \node (n3) at (0, \c)  [dot, label = {left:$\sin (\alpha)$}] {};

        \draw
          (n1) -- (origin) -- (n2)
          pic[%
            "$\alpha$",
            draw=orange,
            <->,
            angle eccentricity=1.2,
            angle radius=2cm,
          ]{angle=n2--origin--n1};

        % Line connecting
        \draw[dashed] (n1) -- (n2);
        \draw[dashed] (n1) -- (n3);

      \end{tikzpicture}
  \end{center}
\end{proof}

\begin{example}
  The regular pentagon can be constructed.
\end{example}

\begin{proof}
  Set $\alpha = \frac{2}{5}\pi$ and let $x = \cos (\alpha), y =  \sin (\alpha)$.
  Then
  \begin{align*}
    1
    & = e^{2 \pi \ii}                                                      \\
    & = e^{5 \alpha \ii}                                                   \\
    & = {(e^{\ii \alpha})}^5                                               \\
    & = (x + \ii y)^5                                                      \\
    & = x^5 + 5 \ii x^4y - 10 x^3 y^2 - 10 \ii x^2 y^3 + 5 x y^4 + \ii y^5 \\
    & = x^5 - 10 x^3 y^2 + 5 x y^4 +\ii
        \underset{=0\text{~by comparing coefficients}}
        {\underbrace{(x^4y -  10 x^2 y^3 + y^5)}}                          \\
    & = x^5 - 10 x^3 y^2 + 5 x y^4                                         \\
    & = x^5 - 10 x^3 (1-x^2) + 5 x (1-x^2)^2                               \\
    & = 11x^5 - 10 x^3 + 5 x (1-2x^2+x^4)                           \\
    & = 11x^5 - 10 x^3 + 5 x - 10 x^3 + 5x^5                        \\
    & = 16 x^5 - 20 x^3 + 5x
  \end{align*}
  so $\cos (\alpha)$ is a root of $p(x) = 16 x^5 - 20 x^3 + 5x - 1 = (x-1)(4x^2
  + 2x - 1)^2$. We can rule out $\cos (\alpha) = 1$, and $\cos (\alpha) > 0$, so
  \begin{align*}
    \cos (\alpha)
    = \frac{-2 + \sqrt{4 + 16}}{8}
    = \frac{1}{8}(-2 + \sqrt{20})
    = \frac{1}{8}(-2 + 2 \sqrt{5})
    = \frac{1}{4}(-1 + \sqrt{5})
  \end{align*}
  which is constructible as a sequence of elementary arithmetic operations and
  root extraction.
\end{proof}

\begin{definition}[Fermat Prime]
  A prime number $p$ is named a \textbf{Fermat Prime} if it is in the form
  \begin{align*}
    p = 2^{2^k} + 1
  \end{align*}
\end{definition}
\begin{remark}
  There are only 5 primes of this type known: 3, 5, 17, 257, 65537.
\end{remark}

\begin{theorem}[Gauss-Wantzel]\label{thm:gauss-wantzel}
  The regular n-gon can be constructed with a straightedge and compass if
  and only if $n$ is the product of a power of two and a Fermat prime, that is
  in the form
  \begin{align*}
    n = 2^k (2^{2^m}+1)
  \end{align*}
\end{theorem}

We will discuss the proof in a later lecture.

\begin{corollary}
  The following constructions are therefore impossible
  \begin{enumerate}[(i)]
    \item (\textbf{Doubling the Cube}) Given a cube of volume $V$ and side length
      $s$, construct a cube of volume $2V$.
    \item (\textbf{Trisecting the Angle}) Given an arbitrary angle $\alpha$,
      construct $\frac{1}{3}\alpha$.
    \item (\textbf{Squaring the Circle}) Construct a square with the same area as
      a given circle.
  \end{enumerate}
\end{corollary}

\begin{proof}[Proof Doubling the cube]
  Without loss of generality, we may set $s=1$, thus $V=1$ and we must construct
  a cube of volume $2$. This is equivalent to constructing a length
  $\sqrt[3]{2}$. On the other hand, $\sqrt[3]{2}$ is the root to $X^3-2$, which
  by Eisenstein is irreducible, hence $\dim (\Q(\sqrt[3]{2}) / \Q) = 3$. This
  isn't a power of two, and thus, $\sqrt[3]{2}$ is not constructible.
\end{proof}

\begin{proof}[Proof trisecting an angle]
  We have for $z = e^{\varphi \ii}$ that
  \begin{align*}
    z^n + z^{-n}
    & = e^{n \varphi \ii} + e^{- n \varphi \ii} \\
    & = \cos (n \varphi)
        + \ii \sin (n \varphi)
        + \cos (-n \varphi)
        + \ii \sin (-n \varphi)                 \\
    & = \cos (n \varphi)
        + \ii \sin (n \varphi)
        + \cos (n \varphi)
        - \ii \sin (n \varphi)                  \\
    & = 2 \cos (n \varphi)
  \end{align*}
  %
  so
  %
  \begin{align*}
    {(2 \cos (\varphi))}^3
    & = {(z + z^{-1})}^3                      \\
    & = z^3 + 3 z + 3 z^{-1} + z^{-3}         \\
    & = (z^3 + z^{-3}) + 3 (z + z^{-1})       \\
    & = 2 \cos (3 \varphi) + 6 \cos (\varphi) \\
    \implies
    \cos (3 \varphi)
    & = 4 \cos (\varphi)^3 - 3 \cos (\varphi) \\
    \implies
    \cos (\varphi)
    & = 4 \cos (\frac{1}{3}\varphi)^3 - 3 \cos (\frac{1}{3}\varphi).
  \end{align*}
  %
  In other words, we have that $\cos (\frac{1}{3}\varphi)$ must be a root of the
  polynomial
  %
  \begin{align*}
     4X^3 - 3X - \cos (\varphi) = 0
  \end{align*}
  %
  However, we know, we can only construct extensions with degree a power of $2$,
  yet this polynomial will create an extension of degree $3$ in general.
\end{proof}

\begin{remark}
  Some angles, of course, are trisectable. For example $\frac{\pi}{2}$, we
  merely need construct an equalateral triangle and bisect one of the angles to
  obtain $\frac{\pi}{6}$.
\end{remark}

\begin{proof}[Proof squaring the circle]
  A circle of radius $1$, has area $\pi$. To then construct a square of area
  $\pi$, we need a side length of $\sqrt{\pi}$. We have $\dim (\Q (\pi) / \Q
  (\sqrt{\pi})) = 2$ and the \textit{highly} non-trivial fact that $\pi$ is
  transcendental i.e. $\dim (\Q(\pi) / \Q) = \infty$. Together, we then have
  that $\dim (\Q(\sqrt{\pi}) / \Q) = \infty$ by rearranging the dimension
  formula.
\end{proof}

