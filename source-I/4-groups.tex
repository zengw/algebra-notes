\chapter{Groups}

\section{Definition of the Group}

\begin{definition}
  A \textbf{group} is a set $G$ equipped with a binary operation $*$ such that
  %
  \begin{enumerate}
    \item the \textbf{associative law} holds: for every $x,y, z \in G$,
      \begin{align*}
      x * (y*z) = (x*y)*z;
      \end{align*}

    \item there is an element $e \in G$, called the \textbf{identity}, with $e*x
      = x = x*e$ for all $x \in G$;

    \item every $x \in G$ has an \textbf{inverse}: there is $x' \in G$ with $x *
      x' = e = x' * x$.
  \end{enumerate}
\end{definition}

\begin{remark}
  It is actually enough for a group to have a left-identity and a left-inverse
  for all $x \in G$. The other equations follow from these axioms.
\end{remark}

\begin{definition}
  A group $G$ is called \textbf{abelian} if it satisfies the \textbf{commutative
  law}.
\end{definition}

\begin{lemma}
  Let $G$ be a group.
  \begin{enumerate}
    \item \textbf{Cancellation Law:} If either $x * a = x*b$ or $a*x = b*x$,
      then $a = b$.
    \item The element $e$ is unique.
    \item Each $x \in G$ has a unique inverse.
    \item $(x^{-1})^{-1}  = x$ for all $x \in G$.
  \end{enumerate}
\end{lemma}

\begin{proposition}[Laws of Exponents]
  Let $G$ be a group, let $a,b \in G$ and $m,n \in \Z$.
  %
  \begin{enumerate}
    \item If $a$ and $b$ commute, then $(ab)^{n} = a^n b^n$.
    \item $(a^m)^n = a^{mn}$.
    \item $a^m a^n = a^{m+n}$.
  \end{enumerate}
\end{proposition}

\begin{definition}
  A \textbf{semigroup} is a set having an associative operation; a
  \textbf{monoid} is a semigroup $S$ having a (two-sided) identity element 1;
  that is, $1s = s= s1$ for all $s \in S$.
\end{definition}

\begin{definition}
  Let $G$ be a group and let $a \in G$. If $a^k = 1$ for some $k \geq 1$, then
  the smallest such exponent $k \geq 1$ is called the \textbf{order} of $a$;
  if no such power exists, then we say that $a$ has \textbf{infinite order}.
\end{definition}

\begin{proposition}
  If $a \in G$ is an element of order $n$, then $a^m = 1$ if and only if $n \mid
  m$.
\end{proposition}

\begin{proposition}
  If $G$ is a finite group, then every $x \in G$ has finite order.
\end{proposition}

\begin{definition}
  If $G$ is a finite group, then the number of elements in $G$, denoted by
  $|G|$, is called the \textbf{order} of $G$.
\end{definition}

\begin{definition}
  If $\pi_{n}$ is a regular polygon with $n \geq 3$ vertices $v_1, \ldots, v_n$
  and center $O$, then the symmetry group $\Sigma(\pi_n)$ is called the
  \textbf{dihedral group} of order $2n$, denoted by $D_{2n}$ or $D_{n}$. The
  \textbf{dihedral group} $D_{4} = \mathbf{V}$, the \textbf{four-group}, is
  defined by
  %
  \begin{align*}
    \mathbf{V}  = \set{(1), (1 \, 2)(3 \, 4), (1 \, 3)(2 \, 4),(1 \, 4)(2 \, 3)}
                \subset S_{4}
  \end{align*}
  %
  and has order 4.
\end{definition}

\begin{remark}
  More generally, the dihedral group $D_{2n}$ of order $2n$ contains the $n$
  rotations $p^j$ about the center by $\frac{2\pi j}{n} \text{rad}, 0 \leq j
  \leq n-1$.  The description of the other $n$ elements depends on the parity of
  $n$.  If $n$ is odd, then the other $n$ symmetries are reflections in the
  distinct lines $Ov_i, 1 \leq i \leq n$. If $n = 2q$ is even, then each line
  $Ov_i$ coincides with the line $Ov_{q + i}$, giving only $q$ such reflections;
  the remaining $q$ symmetries are reflections in the lines $Om_i, 1 \leq i \leq
  q$, where $m_i$ is the midpoint of the edge $v_i v_{i+1}$.
\end{remark}


\section{Subgroups, Cosets and Lagrange's Theorem}

\subsection{Subgroups}

\begin{definition}
  A subset $H$ of a group $G$ is a \textbf{subgroup} if
  %
  \begin{enumerate}
    \item $1 \in H$,
    \item $H$ is \textbf{closed}; that is, if $x,y \in H$, then $xy \in H$,
    \item if $x \in H$, then $x^{-1} \in H$.
  \end{enumerate}
  %
  A subgroup $H \subsetneq G$ is called a \textbf{proper subgroup}; a subgroup
  $H \neq \{1\}$ is called a \textbf{nontrivial subgroup}.
\end{definition}

\begin{proposition}
  A subset $H$ of a group $G$ is a subgroup if and only if $H$ is nonempty and
  $xy^{-1} \in H$ whenever $x,y \in H$.
\end{proposition}

\begin{corollary}
  A nonempty subset $H$ of a \emph{finite} group $G$ is a subgroup if and only
  if $H$ is closed; that is, $x,y \in H$ implies $xy \in H$.
\end{corollary}

\begin{example}
  A counterexample for infinite groups is $\N \subset \Z$. $\Z$ is an additive
  group and $\N$ is closed under addition, but $\N$ is not a subgroup of $\Z$.
\end{example}

\begin{definition}
  If $G$ is a group and $a \in G$, then the \textbf{cyclic subgroup} of $G$
  \textbf{generated} by $a$, denoted by $\langle a \rangle$, is
  %
  \begin{align*}
    \langle a\rangle = \{a^{n}: n \in \Z\}.
  \end{align*}
  %
  A group $G$ is called \textbf{cyclic} if there exists $a \in G$ with $G =
  \langle a \rangle$. Then $a$ is called a \textbf{generator} of $G$.
\end{definition}

\begin{proposition}
  Let $G$ be a group. If $a \in G$, then the order of $a$ is equal to $|\langle
  a \rangle|$.
\end{proposition}

\begin{theorem}
  If $G = \langle a \rangle$ is a cyclic group of order $n$, then $a^k$ is a
  generator of $G$ if and only if $\gcd(k,n)=1$.
\end{theorem}

\begin{lemma}
  A cyclic group $G$ is isomorphic either to $(\Z, +)$ or to $(\Z\setminus m \Z,
  \cdot)$ for some $m \geq 1$.
\end{lemma}

\begin{definition}
  If $X$ is a subset of a group $G$, then $\langle X \rangle$ is called the
  \textbf{subgroup generated by} X.
\end{definition}


\subsection{Cosets and Lagrange's Theorem}

\begin{definition}
  If $H$ is a subgroup of a group $G$ and $a \in G$, then the \textbf{(left)
  coset} $aH$ is
  %
  \begin{align*}
    aH = \{ah:h \in H\}.
  \end{align*}
  %
  Each element of a coset $aH$ is called a \textbf{representative} of it. In
  general, left cosets and right cosets are different, furthermore cosets
  generally are not subgroups.
\end{definition}

\begin{theorem}
  Membership in cosets defines an equivalence relation. In particular, the
  cosets form a partition of the group.
\end{theorem}

\begin{lemma}
  Let $H$ be a subgroup of a group $G$ and let $a,b \in G$.
  %
  \begin{enumerate}
    \item $aH = bH$ if and only if $b^{-1} a \in H$. In particular, $aH = H$ if
      and only if $a \in H$.
    \item If $aH \cap bH \neq \emptyset$, then $aH = bH$.
    \item $|aH| = |H|$ for all $a \in G$.
  \end{enumerate}
\end{lemma}

\begin{definition}
  The \textbf{index} of a subgroup $H$ in $G$, denoted by $[G:H]$, is the number
  of \emph{left} cosets of $H$ in $G$.
\end{definition}

\begin{theorem}[Lagrange's Theorem]
  If $H$ is a subgroup of a finite group $G$, then $|H|$ is a divisor of $|G|$.
  Moreover,
  %
  \begin{align*}
    [G:H] = |G|/|H|.
  \end{align*}
\end{theorem}

\begin{corollary}
  If $G$ is a finite group and $a \in G$, then the order of $a$ is a divisor of
  $|G|$.
\end{corollary}

\begin{corollary}
  If $G$ is a finite group, then $a^{|G|} = 1$ for all $a \in G$.
\end{corollary}

\begin{corollary}
  If $p$ is prime, then every group $G$ of order $p$ is cyclic.
\end{corollary}


\section{Homomorphisms}

\begin{definition}
  Let $(G,*)$ and $(H,\circ)$ be groups. A \textbf{homomorphism} is a function
  satisfying
  %
  \begin{align*}
    \forall x,y \in G: f(x*y) = f(x) \circ f(y)
  \end{align*}
\end{definition}

\begin{lemma}
  Let $f \colon G \to H$ be a homomorphism of groups. Then
  %
  \begin{enumerate}
    \item $f(1) = 1$.
    \item $f(x^{-1}) = f(x)^{-1}$.
    \item $f(x^n) = f(x)^{n}$ for all $n \in \Z$.
  \end{enumerate}
\end{lemma}

\begin{definition}
  If $f \colon G \to H$ is a homomorphism, define the \textbf{kernel} of $f$ by
  %
  \begin{align*}
    \ker f = \set{x \in G: f(x) = 1}
  \end{align*}
  %
  and the \textbf{image} of $f$ by
  %
  \begin{align*}
    \im f = \set{f(x): x \in G}.
  \end{align*}
\end{definition}

\begin{definition}
  Let $G$ be a group. A \textbf{conjugate} of $a \in G$ is an element in $G$ of
  the form $gag^{-1}$ for some $g \in G$.
\end{definition}

\begin{definition}
  If $G$ is a group and $g \in G$, then \textbf{conjugation by} $g$ is the
  function defined by for all $a \in G$
  %
  \begin{align*}
    \gamma_g \colon G & \to G            \\
    a                 & \mapsto gag^{-1}
  \end{align*}
\end{definition}

\begin{proposition}~
  \begin{enumerate}
    \item If $G$ is a group and $g \in G$, then conjugation $\gamma_g: G \to G$
      is an isomorphism.
    \item Conjugate elements have the same order.
  \end{enumerate}
\end{proposition}

\begin{definition}
  A subgroup $K$ of a group $G$ is called a \textbf{normal subgroup} it is
  invariant under conjugation from elements in G. That is, $\forall g \in G:
  \gamma_g (K) = K$. If $K$ is a normal subgroup of $G$, we write $K
  \triangleleft G$.
\end{definition}

\begin{proposition}
  Let $f \colon G \to H$ be a homomorphism.
  %
  \begin{enumerate}
    \item $\ker f$ is a subgroup of $G$ and $\im f$ is a subgroup of $H$.
    \item If $x \in \ker f$ and $a \in G$, then $a x a^{-1} \in \ker f$, thus
      $\ker f \triangleleft G$.
    \item $f$ is an injection if and only if $\ker f = \{1\}$.
  \end{enumerate}
\end{proposition}

\begin{proposition}
  Let $f \colon G \to H$ be a homomorphism and let $x \in G$.
  %
  \begin{enumerate}
    \item If $x$ has (finite) order $k$, then $f(x) \in H$ has order $m$, where
      $m \mid k$.
    \item If $f$ is an isomorphism, then $x$ and $f(x)$ have the same order.
  \end{enumerate}
\end{proposition}

\begin{definition}
  The \textbf{center} of a group $G$, $Z(G)$, is defined by
  %
  \begin{align*}
    Z(G) := \set{z \in G \text{~s.t~} \forall g \in G : zg = gz}.
  \end{align*}
  %
  Thus $Z(G)$ consists of the elements, that commute with everything in $G$.
  $Z(G)$ is a normal subgroup of $G$ and a group $G$ is abelian if and only if
  $Z(G) = G$.
\end{definition}

\begin{example}
  Every subgroup of an Abelian group is normal, however the converse does not
  hold true. The quaternions are a non-abelian group, for which every subgroup
  is normal.
\end{example}

\begin{definition}
  The conjugations are called \textbf{inner automorphisms}. An automorphism that
  is not inner is called \textbf{outer automorphism}. The set $\mathrm{Aut}(G)$
  of all the automorphisms of $G$ is itself a group. Furthermore,
  %
  \begin{align*}
    \mathrm{Inn}(G) = \set{\gamma_g: g \in G}.
  \end{align*}
\end{definition}

\begin{lemma}
  Let $G$ be a group. Then $\mathrm{Inn}(G) \triangleleft \mathrm{Aut}(G)$.
\end{lemma}

\begin{proposition}
  Let $H$ be a subgroup of index 2 in a group $G$.
  %
  \begin{enumerate}
    \item $g^2 \in H$ for every $g \in G$.
    \item $H$ is a normal subgroup of $G$.
  \end{enumerate}
\end{proposition}

\begin{theorem}
  Let $H$ be a subgroup in a group $G$. The following statements are equivalent.
  %
  \begin{enumerate}
    \item $\forall x \in G: xH = Hx$.
    \item $\forall x \in G, \exists y \in G: xH = Hy$.
    \item $\forall x \in G: xHx^{-1} = H$.
    \item $\forall y \in H : H$ contains all of its conjugates $xyx^{-1}, x \in
      G$
    \item There exists a homomorphism between groups $\varphi \colon G \to K$,
      such that $H = \ker \varphi$.
    \item For all $x,y \in G: (xH) \cdot (yH) = xyH$
  \end{enumerate}
\end{theorem}


\section{Quotient Groups}

\begin{definition}[Multiplication of Subsets]
  Let $G$ be a group and let $\mathcal{S}(G)$ be the set of all nonempty subsets
  of $G$. For $X,Y \in \mathcal{S}(G)$ we define
  %
  \begin{align*}
    XY = \set{xy : x \in X, y \in Y}
  \end{align*}
  %
  This multiplication is associative and it is easy to see that $\mathcal{S}(G)$
  is a monoid.
\end{definition}

\begin{lemma}
  A subgroup $K$ of a group $G$ is a normal subgroup if and only if
  %
  \begin{align*}
    gK = Kg
  \end{align*}
  %
  for every $g \in G$. Thus, every right coset of a normal subgroup is also a
  left coset.
\end{lemma}

\begin{proposition} \mbox{}
  \begin{enumerate}
    \item If $H$ and $K$ are subgroups of a group $G$, at least one of which is
      normal, then $HK$ is a subgroup of $G$; moreover, $HK = KH$ in this case.
    \item If both $H$ and $K$ are normal subgroups, then $HK$ is a normal
      subgroup.
  \end{enumerate}
\end{proposition}

\begin{theorem}
  Let $G/K$ denote the family of all left cosets of a subgroup $K$ of $G$. If
  $K$ is a normal subgroup, then
  \begin{align*}
    aKbK = abK
  \end{align*}
  for all $a,b \in G$, and $G/K$ is a group under this operation. This group is
  then called \textbf{quotient group} $G \mod K$.
\end{theorem}

\begin{corollary}
  Every normal subgroup $K \triangleleft G$ is the kernel of some homomorphism,
  namely of the natural map defined by
  \begin{align*}
    \pi \colon G & \to G/K    \\
    a            & \mapsto aK
  \end{align*}
\end{corollary}

\begin{theorem}[First Isomorphism Theorem] \label{thm:first_iso_thm_groups}
  If $f \colon G \to H$ is a homomorphism, then
  %
  \begin{align*}
    \ker f \triangleleft G \text{~and~} G/\ker f \cong \im f.
  \end{align*}
  %
  In more detail, if $\ker f = K$, then $\varphi \colon G/K \to \im f \subseteq
  H$, given by $\varphi: aK \mapsto f(a)$, is an isomorphism.
\end{theorem}

\begin{remark}
  The following diagram describes theorem~\ref{thm:first_iso_thm_groups}, where
  $\pi \colon G \to G/K$ is the natural map $a \mapsto aK$ and $\imath \colon
  \im f \to H$ is the inclusion.
  %
  \begin{figure}[h]
    \centering
    \begin{tikzpicture}\label{fig:first_iso_thm_groups}
      \matrix (m)[matrix of math nodes,row sep=3em,column sep=4em]{%
        G   & H     \\
        G/K & \im f \\
      };

      \path[-stealth]
        (m-1-1) edge node [left] {$\pi$}
        (m-2-1) edge node [above] {$f$} (m-1-2)
        (m-2-1) edge node [below] {$\varphi$} (m-2-2)
        (m-2-2) edge node [right] {$\imath$} (m-1-2);
    \end{tikzpicture}
  \end{figure}
\end{remark}

\begin{proposition}[Product Formula]
  If $H$ and $K$ are subgroups of a finite group $G$, then
  %
  \begin{align*}
    |HK||H\cap K| = |H||K|.
  \end{align*}
\end{proposition}

\begin{theorem}[Second Isomorphism Theorem]
  If $H$ and $K$ are subgroups of a group $G$ with $H \triangleleft G$, then
  $HK$ is a subgroup, $H \cap K \triangleleft K$, and
  %
  \begin{align*}
    K/(H \cap K) \cong HK/H.
  \end{align*}
\end{theorem}

\begin{theorem}[Third Isomorphism Theorem]
  If $H$ and $K$ are normal subgroups of a group $G$ with $K \subseteq H$, then
  $H/K \triangleleft G/K$ and
  \begin{align*}
    (G/K)/(H/K) \cong G/H.
  \end{align*}
\end{theorem}

\begin{theorem}[Correspondence Theorem*]
  Let $G$ be a group, let $K \triangleleft G$, and let $\pi \colon G \to G/K$ be
  the natural map. Then
  %
  \begin{align*}
    S \mapsto \pi(S) = S/K
  \end{align*}
  %
  is a bijection between $\mathrm{Sub}(G;K)$, the family of all those subgroups
  $S$ of $G$ that contain $K$, and $\mathrm{Sub}(G/K)$, the family of all the
  subgroups of $G/K$. Moreover, $T \subseteq S \subseteq G$ if and only if $T/K
  \subseteq S/K$, in which case $[S:T] = [S/K : T/K]$, and $T \triangleleft S$
  if and only if $T/K \triangleleft S/K$, in which case $S/T \cong (S/K)/(T/K)$.
  The following diagram describes the theorem.
  %
  \begin{figure}[h]
    \centering
    \begin{tikzpicture}\label{fig:correspondence_thm}
      \matrix (m) [matrix of math nodes,row sep=0.05em,column sep=4em]{%
        G &         \\
          & G/K     \\
        S &         \\
          & S/K     \\
        T &         \\
          & T/K     \\
        K &         \\
          & \set{1} \\
      };

      \path[-stealth]
        (m-1-1) edge (m-2-2)
        (m-3-1) edge (m-4-2)
        (m-5-1) edge (m-6-2)
        (m-7-1) edge (m-8-2);

        \path[-]
        (m-1-1) edge (m-3-1)
        (m-3-1) edge (m-5-1)
        (m-5-1) edge (m-7-1)
        (m-2-2) edge (m-4-2)
        (m-4-2) edge (m-6-2)
        (m-6-2) edge (m-8-2);
    \end{tikzpicture}
  \end{figure}
\end{theorem}

\begin{proposition*}
  If $G$ is a finite abelian group and $d$ is a divisor of $|G|$, then $G$
  contains a subgroup of order $d$.
\end{proposition*}

\begin{definition*}
  If $H$ and $K$ are groups, then their \textbf{direct product}, denoted by $H
  \times K$, is the set of all ordered pairs $(h,k), h \in H, k \in K$, equipped
  with the operation
  %
  \begin{align*}
    (h,k) (h',k') = (hh', kk')
  \end{align*}
\end{definition*}

\begin{proposition*}
  Let $G$ and $G'$ be groups, and let $K \triangleleft G$ and $K' \triangleleft
  G'$ be normal subgroups. Then $(K \times K') \triangleleft (G \times G')$, and
  there is an isomorphism
  %
  \begin{align*}
    (G \times G')/(K \times K') \cong (G/K) \times (G'/K').
  \end{align*}
\end{proposition*}

\begin{proposition*}
  If $G$ is a group containing normal subgroups $H$ and $K$ with $H \cap K  =
  \{1\}$ and $HK = G$, then $G \cong H \times K$.
\end{proposition*}

\begin{theorem}
  If $m$ and $n$ are relatively prime, then
  \begin{align*}
    \Z_{mn} \cong \Z_{m} \times \Z_{n}.
  \end{align*}
\end{theorem}


\section{Group Actions and the Sylow Theorems}

\begin{definition}
  Let $G$ be a group and $T$ a set. An \textbf{action} of $G$ on $T$ is a map
  %
  \begin{align*}
    a \colon G \times T \to T,
  \end{align*}
  %
  that we denote $a(g,t) = g \cdot t$, such that
  %
  \begin{enumerate}
    \item for all $t \in T$, we have $e_{G} \cdot t$ = t,
    \item for all $t \in T$, all $g_1, g_2 \in G$, we have $g_1 \cdot (g_2 \cdot
      t) = (g_1 g_2) \cdot t$.
  \end{enumerate}
\end{definition}

\begin{definition}
  Let $T$ be a set. Then we define
  \begin{align*}
    \Sym(T) := \set{f \colon T \to T, f \text{~bijective}}
  \end{align*}
\end{definition}

\begin{remark}
  Let $a$ be an action of $G$ on $T$. For $g \in G$, let $\alpha_{g}$ be the map
  from $T$ to $T$ such that $\alpha_{g}(t) = g \cdot t$. It is easy to see that
  $\alpha_{g}$ is a bijection with inverse $\alpha_{g^{-1}}$. Thus, we have a
  group homomorphism $\alpha \colon G \to \Sym(T)$. Conversely, let $\alpha
  \colon G \to \Sym(T)$ be a group homomorphism. Define
  %
  \begin{align*}
    a(g,t) = \alpha(g)(t) \in T.
  \end{align*}
  %
  Then $a$ is a group action of $G$ on $T$.
\end{remark}

\begin{remark}
  The actions of a \emph{fixed} group $G$ on sets form a category. The objects
  are group actions and if $G$ acts on $T_1$ and $T_2$, a morphism $T_1 \to T_2$
  of actions of $G$ is a map of sets $f \colon T_1 \to T_2$ such that
  %
  \begin{align*}
    f(g_1 \cdot t) = g_2 \cdot f(t),
  \end{align*}
  %
  where the first action is in $T_1$ and the second in $T_2$. We call $f$ a
  $G$-morphism. The composition of the morphisms is the standard composition of
  functions.
\end{remark}

\begin{definition}
  If $G$ acts on $T$ and $S \subset T$ is such that $g \cdot s \in S, \forall s
  \in S$, then $G$ also acts on $S$. We call $S$ a
  $\mathbf{G}$\textbf{-invariant subset}. If $S$ consists of a single element
  and is invariant, we call that point a \textbf{fixed point} of the action.
\end{definition}

\begin{definition}
  Let $G$ act on $T$. If the homomorphism $\alpha \colon G \to \Sym(T)$ is
  injective, then the action is called \textbf{faithful}, i.e. $e_{G} \in G$ is
  the only element of $G$ such that $g \cdot t = t, \forall t \in T$.
\end{definition}

\begin{definition}
  Let $G$ act on $T$. Let $t \in T$. The map $\gamma_t \colon G \to T$ such that
  $\gamma_t(g) = g \cdot t$ is called the \textbf{orbit map} associated to $t$.
  Its image defines a set called the $\mathbf{G}$\textbf{-orbit} of $t$ in $T$
  %
  \begin{align*}
    \Orb _G (t) = \gamma_t(G) = \set{g \cdot t \mid g \in G}
  \end{align*}
\end{definition}

\begin{lemma}
  Let $G$ act on $T$. The relation on $T$ defined by $t_1 \sim t_2$ if and only
  if there exists $g \in G$ such that $t_2 = g \cdot t_1$ is an equivalence
  relation. Its equivalence classes are the orbits of $G$ in $T$.
\end{lemma}

\begin{definition}
  Let $G$ act on $T$.
  %
  \begin{enumerate}
    \item The set of $G$-orbits of $T$ is denoted $G \setminus T$.
    \item If all $G$-orbits in $T$ coincide, then we say that the action of $G$
      on $T$ is \textbf{transitive}.
  \end{enumerate}
\end{definition}

\begin{definition}
  Let $G$ be a group and $T$ a non-empty set with a $G$-action. Let $t \in T$.
  We define the \textbf{stabilizer} of $t$ in $G$ to be
  %
  \begin{align*}
    \Stab _G (t) = \set{g \in G \mid g \cdot t = t} \subseteq G,
  \end{align*}
\end{definition}

\begin{theorem}[Orbit-stabilizer Theorem]
  Let $G$ be a group and $T$ a non-empty set with a $G$-action. Let $t \in T$.
  For brevity, denote $\Sigma _t = \Stab _G (t)$ and $\Omega _t = \Orb _G (t)$,
  then
  %
  \begin{enumerate}
    \item The stabilizer $\Sigma _t$ is a subgroup of $G$.

    \item There is a well-defined $G$-morphism $f \colon G/\Sigma _t \to T$
      such that $f(x\Sigma _t) = x \cdot t$ for all $x \in G$.

    \item The map $f$ induces a $G$-isomorphism of $G/ \Sigma _t$ to $\Omega
      _t$. In particular, if $G$ is a finite group, then $\Omega _t$ is finite,
      its cardinality divides $|G|$, and we have
      %
      \begin{align*}
        |G| = |\Omega _t||\Sigma _t| = |\Orb _G (t) ||\Stab _G (t)|
      \end{align*}
  \end{enumerate}
\end{theorem}

\begin{corollary}
  Let $G$ be a finite group acting on a finite set $T$. Let $F$ be the set of
  fixed points of the action. We have
  %
  \begin{align*}
    |T| = |F| + \sum_{x} [G: \Stab_{G}(x)]
  \end{align*}
  %
  where $x$ iters over representatives of the non-trivial orbits of $G$ in $T$.
\end{corollary}

\begin{lemma}
  For $n = p^k m$ with $m$ coprime to $p$, the integer
  %
  \begin{align*}
    n \choose p^k
  \end{align*}
  %
  is \textbf{not} divisible by $p$.
\end{lemma}

\begin{theorem}[Sylow]
  Let $G$ be a finite group of cardinality $n$ and $p$ a prime. Write $n = p^k
  m$, where $k \geq 0$ and $p$ does not divide $m$.
  %
  \begin{enumerate}
    \item There exists a subgroup of cardinality $p^k$ in $G$.
    \item  If $H$ is a subgroup of $G$ of cardinality a power of $p$, then there
      exists a subgroup of $G$ of cardinality $p^k$ that contains $H$.
    \item All subgroups of $G$ of cardinality $p^k$ are conjugate.
  \end{enumerate}
\end{theorem}


\section{The Symmetric Group}

\begin{definition}
  A \textbf{permuation} of a set $X$ is a bijection of $X$ to itself. We denote
  the family of all permutations of a set $X$ by $S_X$. If $X =
  \set{1, 2, \ldots, n}$ we write $S_n$.
\end{definition}

\begin{definition}
  Let $i_1, i_2, \ldots, i_r$ be distinct integers in $X = \{1, 2, \ldots, n\}$.
  If $\alpha \in S_n$ fixes the other integers in $X$ (if any) and if
  %
  \begin{align*}
    \alpha(i_1)     = i_2,
    \alpha(i_2)     = i_3,
    \ldots,
    \alpha(i_{r-1}) = i_r,
    \alpha(i_r)     = i_1,
  \end{align*}
  %
  then $\alpha$ is called an \textbf{r-cycle.} We denote $\alpha$ by
  %
  \begin{align*}
    \alpha = (i_1 \, i_2 \, \ldots \, i_r).
  \end{align*}
  %
  A 2-cycle is called \textbf{transposition}.
\end{definition}

\begin{definition}
  Two permutations $\alpha, \beta \in S_n$ are \textbf{disjoint} if every $i$
  moved by one is fixed by the other.
\end{definition}

\begin{proposition}
  Disjoint permutations commute.
\end{proposition}

\begin{proposition}
  Every permutation $\alpha \in S_n$ is either a cycle or a product of disjoint
  cycles.
\end{proposition}


\begin{proposition} \mbox{}
  \begin{enumerate}
    \item The inverse of the cycle
      %
      \begin{align*}
        \alpha = (i_1 \, i_2 \ldots i_{r-1} \, i_{r})
      \end{align*}
      %
      is the cycle $(i_r \, i_{r-1} \, \ldots \, i_2 \, i_1)$.

    \item If $\gamma \in S_n$ and $\gamma = \beta_1 \hdots \beta_k$, then
      %
      \begin{align*}
        \gamma^{-1} = \beta_{k}^{-1} \hdots \beta_1^{-1}.
      \end{align*}
  \end{enumerate}
\end{proposition}

\begin{definition}
  A \textbf{complete factorization} of a permutation $\alpha$ is a factorization
  of $\alpha$ into disjoint cycles that contains exactly one 1-cycle $(i)$ for
  every $i$ fixed by $\alpha$.
\end{definition}

\begin{theorem}
  Let $\alpha \in S_n$ and let $\alpha = \beta_1 \hdots \beta_t$ be a complete
  factorization into disjoint cycles. This factorization is unique bar the order
  in which the cycles occur.
\end{theorem}

\begin{definition}
  Two permutations have the \textbf{same cycle structure} if, for each $r \geq
  1$, their complete factorizations have the same number of r-cycles.
\end{definition}

\begin{lemma}
  If $\gamma, \alpha \in S_{n}$, then $\alpha \gamma \alpha^{-1}$ has the same
  cycle structure as $\gamma$. In more detail, if the complete factorization of
  $\gamma$ is
  %
  \begin{align*}
    \gamma = \beta_1 \beta_2 \hdots (i_1 \, i_2 \, \ldots \,) \hdots \beta_t,
  \end{align*}
  %
  then $\alpha \gamma \alpha^{-1}$ is the permutation obtained from $\gamma$ by
  applying $\alpha$ to the symbols in the cycles of $\gamma$.
\end{lemma}

\begin{theorem}
  Permutations have the same cycle structure if and only if they are conjugate.
\end{theorem}

\begin{proposition}
  Let $\alpha \in S_{n}$.
  %
  \begin{enumerate}
    \item If $\alpha$ is an $r$-cycle, then $\alpha$ has order $r$.

    \item If $\alpha = \beta_1 \hdots \beta_t$ is a product of disjoint
      $r_i$-cycles $\beta_i$, then the order of $\alpha$ is $\lcm(r_1, \ldots,
      r_t)$.

    \item If $p$ is prime, then $ \alpha$ has order $p$ if and only if it is a
      $p$-cycle or a product of disjoint $p$-cycles.
  \end{enumerate}
\end{proposition}

\begin{proposition}
  If $n \geq 1$, then every $\alpha \in S_n$ is a transposition or a product of
  transpositions.
\end{proposition}

\begin{definition}
  A permutation $\alpha \in S_n$ is \textbf{even} if it is a product of an even
  number of transpositions; $\alpha$ is \textbf{odd} if it is not even. The
  \textbf{parity} of a permutations is whether is is even or odd.
\end{definition}

\begin{definition}
  If $\alpha \in S_n$ and $\alpha = \beta_1 \hdots \beta_t$ is a complete
  factorization into disjoint cycles, then \textbf{signum} $\alpha$ is defined
  by
  %
  \begin{align*}
    \sgn(\alpha) = (-1)^{n-t}.
  \end{align*}
\end{definition}

\begin{theorem}
  For all $\alpha, \beta \in S_n$,
  %
  \begin{align*}
    \sgn(\alpha \beta) = \sgn(\alpha) \sgn(\beta).
  \end{align*}
\end{theorem}

\begin{theorem}~
  \begin{enumerate}
    \item Let $\alpha \in S_n$; if $\sgn(\alpha) = 1$, then $\alpha$ is even, and
      if $\sgn(\alpha) = -1$, then  $\alpha$ is odd.

    \item A permutation $\alpha$ is odd if and only if it is a product of an odd
      number of transpositions.
  \end{enumerate}
\end{theorem}

\begin{lemma}
  Let $G$ be a finite group. Then $G$ is isomorphic to a subgroup of $S_{n}$,
  where $n = |G|$.
\end{lemma}

\begin{definition}
  The subset
  %
  \begin{align*}
    A_n = \set{\alpha \in S_n : \alpha \text{~is even}}
  \end{align*}
  %
  is called the \textbf{alternating group}.
\end{definition}


\section{Simple Groups}

\begin{definition}
  A group $G$ is called \textbf{simple} if $G \neq \set{1}$ and $G$ has no normal
  subgroups other than $\set{1}$ and $G$ itself.
\end{definition}

\begin{proposition}
  An abelian group $G$ is simple if and only if it is finite and of prime order.
\end{proposition}

\begin{lemma}
  Every element in $A_5$ is a 3-cycle or a product of 3-cycles.
\end{lemma}

\begin{lemma}
  Let $H \not = \set{1}$ be a normal subgroup of $A_5$.
  %
  \begin{enumerate}
    \item $H$ contains a 3-cycle.
    \item All 3-cycles are conjugate in $A_5$.
  \end{enumerate}
\end{lemma}

\begin{theorem}
  $A_5$ is a simple group.
\end{theorem}

\begin{theorem}
  $A_6$ is a simple group.
\end{theorem}

\begin{theorem}
  $A_n$ is a simple group for all $n \geq 5$.
\end{theorem}
