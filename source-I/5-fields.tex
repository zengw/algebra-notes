\chapter{Fields}

\section{Field Extensions and Algebraic Elements}

\begin{proposition}
  If $k$ is a field and $I = (f)$, where $f(x)$ is a nonzero polynomial in
  $k[x]$, then the following are equivalent:
  \begin{enumerate}
    \item $f$ is irreducible;
    \item $k[x]/I$ is a field;
    \item $k[x]/I$ is a domain.
  \end{enumerate}
\end{proposition}

\begin{proposition}
  Let $k$ be a field, let $p(x)$ be a monic irreducible polynomial in $k[x]$
  with $\deg(p)  = d$, let $K = k[x] / I$, where $I = (p)$ and let $\beta = x +
  I \in K$. Then:
  %
  \begin{enumerate}
    \item $K$ is a field and $k' = \{a + I: a \in k\}$ is a subfield of $K$
      isomorphic to k. (Hence, if $k'$ is identified with $k$ via $a \mapsto a +
      I$, then $k$ is  a subfield of $K.)$

    \item $\beta$ is a root of $p$ in $K$.

    \item If $g(x) \in k[x]$ and $\beta$ is a root of $g$ in $K$, then $p \mid
      g$ in $k[x]$.

    \item $p$ is the unique monic irreducible polynomial in $k[x]$ having
      $\beta$ as a root.

    \item The list $1, \beta, \beta^{2}, \ldots, \beta^{d-1}$ is a basis of $K$
      as a vector space over $k$, and so $\dim_{k}(K) = d$.
  \end{enumerate}
\end{proposition}

\begin{definition}
  If $K$ is a field containing $k$ as a subfield, then $K$ is called an
  \textbf{extension field} of $k$, denoted by $K/k$. An extension field $K/k$ is
  a \textbf{finite extension} if $K$ is a finite-dimensional vector space over
  $k$. The dimension of $K$, denoted by $[K:k]$, is called the \textbf{degree}
  of $K/k$.
\end{definition}

\begin{definition}
  Let $K/k$ be an extension field. An element $\alpha \in K$ is
  \textbf{algebraic} over $k$ if there is some nonzero polynomial $f(x) \in
  k[x]$ having $\alpha$ as a root; otherwise, $\alpha$ is
  \textbf{transcendental} over $k$. An extension field $K/k$ is
  \textbf{algebraic} if every $\alpha \in K$ is algebraic over $k$.
\end{definition}

\begin{remark}
  When we colloquially refer to a ``transcendental'' number, we usually mean a
  number transcendental over $\Q$.
\end{remark}

\begin{proposition}
  If $K/k$ is a finite extension field, then $K/k$ is an algebraic extension.
\end{proposition}

\begin{definition}
  If $K/k$ is an extension field and $\alpha \in K$, then $k(\alpha)$ is the
  intersection of all those subfields of $K$ containing $k$ and $\alpha$; we
  call $k(\alpha)$ the subfield of $K$ obtained by \textbf{adjoining} $\alpha$
  to $k$.
\end{definition}

\begin{theorem}~
  \begin{enumerate}
    \item If $K/k$ is an extension field and $\alpha \in K$ is algebraic over
      $k$, then there is a unique monic irreducible polynomial $p(x) \in k[x]$
      having $\alpha$ as a root. Moreover, if $I = (p)$, then $k[x] / I \cong
      k(\alpha)$; indeed, there exists an isomorphism
      %
      \begin{align*}
        \varphi:k[x]/I \to k(\alpha)
      \end{align*}
      %
      with $\varphi(x + I) = \alpha$ and $\varphi(c + I) = c$ for all $c \in k$.

    \item If $\alpha' \in K$ is another root of $p(x)$, then there is an
      isomorphism
      %
      \begin{align*}
        \theta: k(\alpha) \to k(\alpha')
      \end{align*}
      %
      with $\theta(\alpha) = \alpha'$ and $\theta(c) = c$ for all $c \in k$.
  \end{enumerate}
\end{theorem}

\begin{definition}
  If $K/k$ is an extension field and $\alpha \in K$ is algebraic over $k$, then
  the unique monic irreducible polynomial $p(x) \in k[x]$ having $\alpha$ as a
  root is called the \textbf{minimal polynomial} of $\alpha$ over $k$; it is
  denoted by $\irr(\alpha,k)$.
\end{definition}

\begin{theorem}
  Let $k \subseteq E \subseteq K$ be fields, with $E$ a finite extension field
  of $k$ and $K$ a finite extension field of $E$. Then $K$ is a finite extension
  field of $k$ and
  \begin{align*}
    [K:k] = [K:E][E:k].
  \end{align*}
\end{theorem}


\section{Splitting Fields and Finite Fields}

\begin{theorem}[Kronecker]
  If $k$ is a field and $f(x) \in k[x]$, there exists an extension field $K/k$
  with $f$ a product of linear polynomials in $K[x]$.
\end{theorem}

\begin{definition}
  If $K/k$ is an extension field and $f(X) \in k[x]$ is nonconstant, then $f$
  \textbf{splits over} $K$ if $f(x) = a(x-z_1) \hdots (x-z_n)$, where $z_1,
  \ldots, z_n$ are in $K$ and $a \in k$. An extension field $E/k$ is called a
  \textbf{splitting field} of $f$ \textbf{over} $k$ if $f$ splits over $E$, but
  $f$ does not split over any proper subfield of $E$.
\end{definition}

\begin{corollary}
  If $k$ is a field and $f(x) \in k[x]$, then a splitting field of $f$ over $k$
  exists.
\end{corollary}

\begin{proposition}
  Let $p$ be prime, and let $k$ be a field. If $f(x) = x^p - c \in k[x]$ and
  $\alpha$ is a pth root of $c$ (in some splitting field), then either $f$ is
  irreducible in $k[x]$ or $c$ has a pth root in $k$. In either case, if $k$
  contains the pth roots of unity, then $k(\alpha)$ is a splitting field of $f$.
\end{proposition}

\begin{theorem}[Galois]
  If $p$ is prime and $n \in \N$, then there exists a field having exactly $p^n$
  elements.
\end{theorem}

\begin{definition}
  If $k$ is a finite field, a generator of the cyclic group $k^{\times}$ is
  called a \textbf{primitive element} of $k$.
\end{definition}

\begin{corollary}
  For every prime $p$ and every integer $n \geq 1$, there exists an irreducible
  polynomial $g(x) \in \mathbb{F}_{p}[x]$ of degree $n$. In fact, if $\alpha$ is
  a primitive element of $\mathbb{F}_{p^n}[x]$, then its minimal polynomial
  $g(x) = \irr(\alpha, \mathbb{F}_{p})$ has degree $n$.
\end{corollary}

\begin{lemma}\label{lem:isomorphism_ext}
  Let $\varphi: k \to k'$ be an isomorphism of fields, and let $\varphi_{*}:k[x]
  \to k'[x]$ be the ring isomorphism
  %
  \begin{align*}
    \varphi_{*}: g(x)
      = a_0 + a_1 x + \cdots + a_n x^n \mapsto g'(x)
      = \varphi(a_0) + \varphi(a_1) x + \cdots + \varphi(a_n) x^n.
  \end{align*}
  %
  Let $f(x) \in k[x]$ and $f'(x) = \varphi_{*}(f) \in k'[x]$. If $E$ is a
  splitting field of $f$ over $k$ and $E'$ is a splitting field of $f'$ over
  $k'$, then there exists an isomorphism $\Phi: E \to E'$ extending $\varphi$:
  %
  \begin{figure}[ht]
    \centering
    \begin{tikzpicture} \label{fig:isomorphism_ext}
      \matrix (m) [matrix of math nodes,row sep=3em,column sep=3em]{%
        E & E' \\
        k & k' \\
        };

      \path[-]
        (m-1-1) edge (m-2-1)
        (m-1-2) edge (m-2-2);

      \path[-stealth]
        (m-2-1) edge node[below] {$\varphi$} (m-2-2);

      \path[dashed, -stealth]
        (m-1-1) edge node[above] {$\Phi$} (m-1-2);
    \end{tikzpicture}

    \caption{Illustration of lemma~\ref{lem:isomorphism_ext}}

  \end{figure}
\end{lemma}

\begin{theorem}
  If $k$ is a field and $f(x) \in k[x]$, then any two splitting fields of $f$
  over $k$ are isomorphic via an isomorphism that fixes $k$ pointwise.
\end{theorem}

\begin{corollary}[Moore]
  Any two finite fields having exactly $p^n$ elements are isomorphic.
\end{corollary}


\section{Algebraic Closure}

\begin{definition}
  A field $K$ is \textbf{algebraically closed} if any non-constant  $f(x) \in
  K[x]$ splits in linear factors over $K$, or equivalently, any non-constant
  $f(x) \in K[x]$ has a root in $K$.
\end{definition}

\begin{definition}
  Let $K$ be a field. An \textbf{algebraic closure} of $K$ is an algebraic
  extension $L/K$ such that $L$ is algebraically closed.
\end{definition}

\begin{example}
  $\C$ is an algebraic closure of $\R$ but not of $\mathbb{Q}$.
\end{example}

\begin{theorem}
  Let $K$ be a field. Then there exists an algebraic closure $\overline{K}$ of
  $K$.
\end{theorem}

\begin{definition}
  If $F/k$ and $K/k$ are extension fields, then a k-\textbf{map} is a ring
  homomorphism $\varphi: F \to K$ that fixes $k$ pointwise.
\end{definition}

\begin{theorem}
  Any two algebraic closures of a field $k$ are isomorphic via a $k$-map.
\end{theorem}

\noindent
It is thus permissible to speak of \emph{the} algebraic closure of a field.
