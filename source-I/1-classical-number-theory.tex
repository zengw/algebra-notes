\chapter{Classical Number Theory}

\section{Divisibility}

\begin{remark}
  We assume that $\N$ satisfies the \textbf{Least Integer Axiom} (also
  called the \emph{Well-Ordering Principle}): Every nonempty subset $C \subseteq
  \N$ contains a smallest element; that is, there is $c_0 \in C$ with
  $c_0 \leq c$.
\end{remark}

\begin{lemma}
  If $a$ and $b$ are positive integers and $a | b$, then $a \leq b$.
\end{lemma}

\begin{theorem}[Division Algorithm]
  If $a$ and $b$ are integers with $a \neq 0$, then there are unique integers
  $q$ and $r$, called the \textbf{quotient} and \textbf{remainder}, with
  %
  \begin{align*}
    b = qa + r \text{~and~} 0 \leq r < |a|
  \end{align*}
\end{theorem}

\begin{definition}[Common Divisor, Greatest common divisor]
  A \textbf{common divisor} of integers $a$ and $b$ is an integer $c$ with $c |
  a$ and $c | b$.  The \textbf{greatest common divisor} of $a$ and $b$, denoted
  by $\mathrm{gcd}(a,b)$, is defined by
  %
  \begin{align*}
    \gcd(a,b) =
    \begin{cases}
      0 \text{~if~} a = 0 = b                                    \\
      \text{the largest common divisor of $a$ and $b$ otherwise}
    \end{cases}
  \end{align*}
\end{definition}

\begin{theorem}
  If $a$ and $b$ are integers, then $\gcd(a,b)$ is a linear combination of $a$
  and $b$.
\end{theorem}

\begin{corollary}
  Let $a$ and $b$ be integers. A non-negative common divisor $d$ is their $\gcd$
  if and only if $c |d$ for every common divisor $c$ of $a$ and $b$.
\end{corollary}

\begin{definition}[Prime]
  An integer $p$ is \textbf{prime} if $p \geq 2$ and its only divisors are $\pm
  1$ and $\pm p$. If an integer $a \geq 2$ is not prime, then it is called
  \textbf{composite}.
\end{definition}

\begin{proposition}
  Every integer $a \geq 2$ has a factorization
  %
  \begin{align*}
    a = p_1 \dotsm p_t,
  \end{align*}
  %
  where $p_1 \leq \ldots \leq p_t$ and all $p_i$ are prime.
\end{proposition}

\begin{definition}[Prime factorization]
  If $a \geq 2$ is an integer, then a \textbf{prime factorization} of $a$ is
  %
  \begin{align*}
    a = p_1^{e_1}p_2^{e_2} \hdots p_t^{e_t},
  \end{align*}
  %
  where the $p_i$ are distinct primes and $e_i \geq 0$ for all $i$.
\end{definition}

\begin{theorem}[Euclid's Lemma]
  If $p$ is a prime and $p \mid ab$, for integers $a$ and $b$, then $p \mid a$
  or $p \mid b$. More generally, if $p \mid a_1 \hdots a_t$, then $p\mid a_i$
  for some $i$.

  Conversely, if $m \geq 2$ is an integer such that $m \mid ab$ always implies
  $m \mid a$ or $m \mid b$, then $m$ is prime.
\end{theorem}

\begin{definition}[Relatively Prime, Coprime]
  We call integers $a$ and $b$ \textbf{relatively prime} if their $\gcd$ is $1$.
  We also call $a$ and $b$ \textbf{coprime}.
\end{definition}

\begin{corollary}
  Let $a,b$ and $c$ be integers. If $c$ and $a$ are relatively prime and if
  $c\mid ab$, then $c\mid b$.
\end{corollary}

\begin{lemma}
  Let $a$ and $b$ be integers.
  %
  \begin{enumerate}[(i)]
    \item Then $\gcd(a,b) = 1$ if and only if $1$ is a linear combination
      of $a$ and $b$.

    \item If $d = \gcd(a,b)$ then the integers $a/d$ and $b/d$ are
      relatively prime.
  \end{enumerate}
\end{lemma}

\begin{theorem}[Fundamental Theorem of Arithmetic]
  Every integer $a \geq 2$ has a unique factorization
  %
  \begin{align*}
    a = p_1^{e_1} \hdots p_t^{e_t},
  \end{align*}
  %
  where $p_1 < \ldots < p_t$, all $p_i$ are prime, and all $e_i > 0$.
\end{theorem}

\begin{corollary}
  If $a = p_1^{e_1} \hdots p_t^{e_t}$ and $b = p_1^{f_1} \hdots p_t^{f_t}$ are
  prime factorizations, then $a \mid b$ if and only if $e_i \leq f_i$ for all
  $i$.
\end{corollary}

\begin{proposition}
  Let $g$ and $h$ be divisors of $a$. If $\gcd(g,h) = 1$, then $gh\mid a$.
\end{proposition}

\begin{definition}[Common multiple, Least common multiple]
  If $a,b$ are integers, then a \textbf{common multiple} is an integer $m$ with
  $a \mid m$ and $b \mid m$. Their \textbf{least common multiple}, denoted by
  %
  \begin{align*}
    \lcm(a,b),
  \end{align*}
  %
  is their smallest common multiple. This definition extends in the obvious way
  to give the $\lcm$ of integers $a_1, \ldots, a_n$.
  \end{definition}

\begin{proposition}
  If $a = p_1^{e_1} \hdots p_t^{e_t}$ and $b = p_1^{f_1} \hdots p_t^{f_t}$ are
  prime factorizations, then
  %
  \begin{align*}
    \gcd(a,b) = p_1^{m_1} \hdots p_t^{m_t} \text{~and~} \lcm(a,b)
              = p_1^{M_1} \hdots p_t^{M_t},
  \end{align*}
  %
  where $m_i = \min\{e_i, f_i\}$ and $M_i = \max\{e_i, f_i\}$.
\end{proposition}

\begin{corollary}
  If $a$ and $b$ are integers, then
  %
  \begin{align*}
    ab = \gcd(a,b) \lcm(a,b).
  \end{align*}
\end{corollary}


\section{Euclidean Algorithm}

\begin{lemma}~
  \begin{enumerate}
    \item If $b = qa + r$, then $\gcd(a,b) = \gcd(r,a)$.
    \item If $b \geq a$ are integers, then $\gcd(a,b) = \gcd(b-a,a)$.
  \end{enumerate}
\end{lemma}


\section{Congruence}

\begin{definition}[Congruent]
  Let $m\leq 0$ be fixed. Then integers $a$ and $b$ are \textbf{congruent
  modulo} $m$, denoted by
  %
  \begin{align*}
    a \equiv b \mod m,
  \end{align*}
  %
  if $m \mid (a-b)$.
\end{definition}


\begin{proposition}[Congruence defines an equivalence relation]
  If $m\geq 0$ is a fixed integer, then for all integers $a,b,c$:
  %
  \begin{enumerate}
    \item $a \equiv a \mod m;$
    \item if $a \equiv b \mod m$, then $b \equiv a \mod m;$
    \item if $a \equiv b \mod m$ and $ b \equiv c \mod m$, then $a \equiv c \mod
      m$,
  \end{enumerate}
\end{proposition}

\begin{proposition}[Elementary properties of congruence]
  Let $m \geq 0$ be a fixed integer.
  %
  \begin{enumerate}
    \item If $a = qm + r$, then $a \equiv r \mod m$.

    \item If $0 \leq r' < r < m$, then $r \not \equiv r' \mod m$; that is, $r$
      and $r'$ are not congruent $\mod m$.

    \item $a  \equiv  b \mod m$ if and only if $a$ and $b$ leave the same
      remainder after dividing by $m$.

    \item If $m \geq 2$, each $a \in \Z$ is congruent $\mod m$ to exactly one of
      $0,1,\ldots, m -1$.
  \end{enumerate}
\end{proposition}

\begin{proposition}[Addition and multiplication]
  Let $m \geq 0$ be a fixed integer.
  \begin{enumerate}
    \item If $a \equiv a' \mod m$ and $b \equiv b' \mod m$, then
      %
      \begin{align*}
        a + b  \equiv  a' + b' \mod m.
      \end{align*}

    \item If $a  \equiv  a' \mod m$ and $b  \equiv  b' \mod m$, then
      %
      \begin{align*}
        ab  \equiv  a'b' \mod m.
      \end{align*}

    \item If $a  \equiv  b \mod m$, then $a^n  \equiv  b^n \mod m$ for all $n
      \geq 1$.
  \end{enumerate}
\end{proposition}

\begin{proposition}~
  \begin{enumerate}
    \item If $p$ is prime, then $p \mid {p \choose r}$ for all $r$ with $0 < r <
      p$, where $\binom p r$ is the binomial coefficient.

    \item For integers $a$ and $b$,
      %
      \begin{align*}
        (a+ b)^p  \equiv  a^p + b^p \mod p.
      \end{align*}
  \end{enumerate}
\end{proposition}

\begin{theorem}[Fermat]
  If $p$ is prime, then
  %
  \begin{align*}
    a^p  \equiv  a \mod p
  \end{align*}
  %
  for every $a$ in $\Z$. More generally, for every integer $k \geq 1$,
  %
  \begin{align*}
    a^{p^{k}} \equiv  a \mod p.
  \end{align*}
\end{theorem}

\begin{corollary}
  If $p$ is prime and $m  \equiv 1 \mod (p-1)$, then $a^m  \equiv  a \mod p$ for
  all $a \in \Z$.
\end{corollary}

\begin{theorem}
  If $\gcd(a,m) = 1$, then, for every integer $b$, the congruence
  %
  \begin{align*}
    ax  \equiv b \mod m
  \end{align*}
  %
  can be solved for $x$; in fact, $x = sb$, where $sa  \equiv  1 \mod m$, is one
  solution. Moreover, any two solutions are congruent $\mod m$.
\end{theorem}

\begin{theorem}[Chinese Remainder Theorem]
  If $m$ and $m'$ are relatively prime, then the two congruences
  %
  \begin{align*}
    x & \equiv  b \mod m   \\
    x & \equiv  b' \mod m' \\
  \end{align*}
  %
  have a common solution, and any two solutions are congruent $\mod mm'$.
\end{theorem}
