\chapter{Excursus to Zorn's Lemma and Categories}

\section{The Axiom of Choice}

Let $I$ be an index set. For every $i \in I$, let $X_i$ be a nonempty set. Then
%
\begin{align*}
  \prod_{i \in I}X_i
\end{align*}
%
is nonempty, i.e.\ there exists $f: I \to \bigcup_{i \in I} X_i$, such
that $f(i) \in X_i$ for each $i \in I$.

\paragraph{Objection.} If $X_i \subset X$, then $f: I \to X$ chooses some
element $x_i = f(i)$ of $X_i$ for each $i$. How is this choice made?

\begin{theorem}
  This cannot be deduced from the other axioms of set theory if it is consistent.
\end{theorem}

\begin{definition}[Ordering]
  Let $X$ be a set. An \textbf{ordering} of $X$ is a relation $\cR$ on
  $X$, such that
  %
  \begin{enumerate}
    \item $x \cR x$
    \item $(x\cR y \text{ and } y \cR x) \implies x = y$
    \item $(x \cR  y \text{ and } y \cR  z) \implies x \cR z$.
  \end{enumerate}
\end{definition}

\begin{definition}[Maximal element]
  Let $X$ be a set and $\cR$ an ordering on $X$. $x \in X$ is called a
  \textbf{maximal element} of $X$, if $x \cR y \implies y = x$.
\end{definition}

\begin{definition}[Total]
  An order $\cR$ on $X$ is \textbf{total} if for all $x,y \in X$ either $x \cR
  y$ or $y \cR x$.
\end{definition}

\begin{remark}
  For a maximal element $x \in X$ with a relation $\cR$ it does not always hold
  that $y \cR x$ for all $y \in X$. Furthermore, a maximal element is not
  necessarily unique.
\end{remark}

\begin{definition}[Partial order]
  A set $X$ is \textbf{Partially ordered} (a poset) if there is a relation $x
  \preceq y$ defined on $X$ which is
  %
  \begin{enumerate}
    \item \textbf{Reflexive:} $x \preceq x$ for all $x \in X$;

    \item \textbf{Anti-Symmetric:} if $x \preceq y$ and $y \preceq x$, then $x =
      y$;

    \item \textbf{Transitive:} if $x \preceq y$ and $y \preceq z$, then $x
      \preceq z$.
  \end{enumerate}
\end{definition}

\begin{definition}[Chain, Simply ordered, Totally ordered]
  A poset $X$ is a \textbf{chain} (or is \textbf{simply ordered} or is
  \textbf{totally ordered}) if, for all $x,y \in X$, either $x \preceq y$ or $y
  \preceq x$. An \textbf{upper bound} of a nonempty subset $Y$ of a poset $X$ is
  an element $x_0 \in X$, not necessarily in $Y$,with $y \preceq x_0$ for every
  $y \in Y$.
\end{definition}

\begin{theorem}[Zorn's Lemma]
  If $X$ is a nonempty poset in which every chain has an upper bound in $X$,
  then $X$ has a maximal element.
\end{theorem}

\begin{lemma}
  If $C$ is a chain in a poset $X$ and $S = \{c_1, \dotsc, c_n\}$ is a finite
  subset of $C$, then there exists some $c_i$ with $c_j \preceq c_i$ for all
  $c_j \in S$.
\end{lemma}

\begin{theorem}
  The Axiom of Choice and Zorn's Lemma are equivalent.
\end{theorem}

\begin{theorem}
  Let $K$ be a field. Let $V$ be a $K$-vector space. Then $V$ has a basis.
\end{theorem}


\section{The Language of Categories and Functors}

\begin{definition}[Category]
  A \textbf{category} $\mathcal{C}$ consists of three ingredients:
  %
  \begin{enumerate}
    \item A class $\Obj(\mathcal{C})$ of \textbf{objects},

    \item A set of \textbf{morphisms} (or \textbf{arrows}) $\Hom(A,B)$ for every
      ordered pair $(A,B)$ of objects

    \item A \textbf{composition} $\Hom(A,B) \times \Hom(B,C) \to
      \Hom(A,C)$ denoted by
      %
      \begin{align*}
        (f,g) \mapsto gf,
      \end{align*}
      %
      for every ordered triple $(A,B,C)$ of objects.
  \end{enumerate}
  %
  These ingredients have to satisfy the following axioms:
  %
  \begin{enumerate}
    \item $\Hom$ sets are pairwise disjoint; that is, each morphism $f \in
      \Hom(A,B)$ has a unique \textbf{domain} $A$ and unique \textbf{target}
      $B$.

    \item For each object $A$, there is an \textbf{identity morphism} $\id_A \in
      \Hom(A,A)$ such that
      %
      \begin{align*}
        \forall f: A \to B: f \id_A = f \text{~and~} \id_B f = f
      \end{align*}

    \item Composition is associative: given morphisms
      %
      \begin{align*}
        A \xrightarrow{f} B \xrightarrow{g} C \xrightarrow{h} D,
      \end{align*}
      %
      we have
      %
      \begin{align*}
          h(gf) = (hg)f.
      \end{align*}
  \end{enumerate}
\end{definition}

\begin{definition}[Isomorphism]
  Let $\mathcal{C}$ be a category. $f: A \to B$ (in $\mathcal{C}$) is an
  \textbf{isomorphism} if there exists $g: B \to A$ in $\mathcal{C}$
  such that
  %
  \begin{align*}
    gf = \id_A \text{~and~} fg = \id_B.
  \end{align*}
  %
  $g$ is called the \textbf{inverse} of $f$.

\end{definition}

\begin{proposition}~
  \begin{enumerate}
    \item $g$ is unique if it exists.

    \item $\id_{A}$ is an isomorphism with $\id_{A}^{-1} = \id_{A}$.

    \item If $f$ and $g$ are isomorphisms, then $f \circ g$ and $(f \circ
      g)^{-1} = g^{-1} \circ f^{-1}$ are also isomorphisms.
  \end{enumerate}
\end{proposition}

\begin{definition}[Functor]
  If $\mathcal{C}$ and $\mathcal{D}$ are categories, then a \textbf{functor} $T:
  \mathcal{C} \to \mathcal{D}$ is a function such that
  %
  \begin{enumerate}
    \item if $A \in \Obj(\mathcal{C})$, then $T(A) \in \Obj(\mathcal{D})$;

    \item if $f: A \to A'$ in $\mathcal{C}$, then $T(f): T(A)
      \to T(A')$ in $\mathcal{D}$;

    \item if $A \overset{f}{\to} A' \overset{g}{\to} A''$ in $\mathcal{C}$, then
      $T(A) \overset{T(f)}{\to} T(A') \overset{T(g)}{\to} T(A'')$ in
      $\mathcal{D}$ and
      %
      \begin{align*}
        T(gf) = T(g) T(f);
      \end{align*}

    \item for every $A \in \Obj(\mathcal{C})$,
      %
      \begin{align*}
        T(1_{A}) = \id_{T(A)}
      \end{align*}
  \end{enumerate}
\end{definition}
